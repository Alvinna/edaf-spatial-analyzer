from __future__ import absolute_import
from __future__ import print_function

# Avoid importing "expensive" modules here (e.g. scipy), since this code is
# executed on PyMOL's startup. Only import such modules inside functions.

import os
from pymol.cgo import *

def __init_plugin__(app=None):
    '''
    Spatial Analyzer
    '''
    from pymol.plugins import addmenuitemqt
    addmenuitemqt('EDAF Spatial Analyzer', run_plugin_gui)


# global reference to avoid garbage collection of our dialog
dialog = None


def run_plugin_gui():
    '''
    Spatial Analyzer Dialog
    '''
    global dialog

    if dialog is None:
        dialog = make_dialog()

    dialog.show()


def make_dialog():
    # entry point to PyMOL's API
    from pymol import cmd

    # pymol.Qt provides the PyQt5 interface, but may support PyQt4
    # and/or PySide as well
    from pymol.Qt import QtWidgets
    from pymol.Qt.utils import loadUi
    from pymol.Qt.utils import getSaveFileNameWithExt

    # create a new Window
    dialog = QtWidgets.QDialog()

    # populate the Window from our *.ui file which was created with the Qt Designer
    uifile = os.path.join(os.path.dirname(__file__), 'main.ui')
    form = loadUi(uifile, dialog)
    
    def analyze():

        import pandas as pd
        import numpy as np
        from rdkit.Chem import PandasTools
        from rdkit import Chem
        from rdkit.Chem import rdDepictor
        from rdkit.Chem.Draw import rdMolDraw2D
        from rdkit.Chem import rdFMCS
        from rdkit.Chem import AllChem
        from rdkit.Chem import ChemicalFeatures
        from rdkit import RDConfig
        from collections import OrderedDict

        store = pd.HDFStore(str(form.text2.text()))
        ligands = Chem.SDMolSupplier(str(form.text1.text()), sanitize=False)

        # Read Feature File
        fdef_name = os.path.join(RDConfig.RDDataDir,'BaseFeatures.fdef')
        fdef = ChemicalFeatures.BuildFeatureFactory(fdef_name)
        #fdef.GetFeatureDefs()
        families = fdef.GetFeatureFamilies()
        res = dict()
        for f in families:
            res[f] = []

        for l in ligands:
            entry = l.GetProp("_Name") + "," + l.GetProp("Mode")

            Chem.SanitizeMol(l, Chem.SANITIZE_ALL-Chem.SANITIZE_PROPERTIES)
            feats = fdef.GetFeaturesForMol(l)
            for feat in feats:
                type, pos, atoms = (feat.GetFamily(), list(feat.GetPos()), [x + 1 for x in feat.GetAtomIds()])
                t1 = store[f"/ligands/{entry}/interaction"]
                interaction = list(t1[t1["lid"].isin(atoms)].sum()[["st", "hy", "hb"]])
                res[type].append((pos, interaction))

        def color_map(interaction):
            r = abs(interaction[0]) * 128
            g = abs(interaction[1]) * 128
            b = abs(interaction[2]) * 128
            return (r, g, b)

        for k, v in res.items():
            t1 = []
            t2 = []
            t3 = []
            t4 = []
            t5 = []
            t6 = []
            obj = [ BEGIN, POINTS ]
            for i in v:
                t1.append(i[0][0])
                t2.append(i[0][1])
                t3.append(i[0][2])
                c = color_map(i[1])
                t4.append(int(c[0]))
                t5.append(int(c[1]))
                t6.append(int(c[2]))
                
                obj.append( COLOR )
                obj.append( int(c[0]) )
                obj.append( int(c[1]) )
                obj.append( int(c[2]) )
                obj.append( VERTEX )
                obj.append( i[0][0] )
                obj.append( i[0][1] )
                obj.append( i[0][2] )
            obj.append( END )
                    
            cmd.load_cgo( obj, k )
            output = pd.DataFrame([t1, t2, t3, t4, t5, t6], index=["x", "y", "z", "r", "g", "b"]).T
            print(k)
            #print(output)
            #output.to_csv(k + ".csv")

    def browse1():
        dlg = QtWidgets.QFileDialog()
        dlg.setFileMode(QtWidgets.QFileDialog.AnyFile)
        if dlg.exec_():
            filename = dlg.selectedFiles()[0]
        form.text1.setText(str(filename))
        
    def browse2():
        dlg = QtWidgets.QFileDialog()
        dlg.setFileMode(QtWidgets.QFileDialog.AnyFile)
        if dlg.exec_():
            filename = dlg.selectedFiles()[0]
        form.text2.setText(str(filename))
      
    
    form.analyze.clicked.connect(analyze)

    form.browse1.clicked.connect(browse1)
    form.browse2.clicked.connect(browse2)
    return dialog
